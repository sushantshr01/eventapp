# Event App

## Setting up development environment

    // make sure composer, docker and docker compose is installed in the host machine

    $ composer install
    $ cp .env.example .env
    $ ./vendor/bin/sail up
    $ docker compose up -d
    $ ./vendor/bin/sail npm install
    $ ./vendor/bin/sail npm run dev
    $ chmod 777 -R storage # to fix the permission in docker
    $ docker compose exec app php artisan migrate --seed
    $ curl -I localhost:9000 # if status is 200 we are good


## Running the tests

- Run unit and feature test

```
./vendor/bin/sail artisan test
```

## Running phpstan

- PHP Static Analysis Tool
- [PHPStan](https://github.com/phpstan/phpstan)

```
./vendor/bin/phpstan analyse app database tests --memory-limit=2G
```

## Running eslint

```
./node_modules/.bin/eslint --ext .js,.vue resources/js
```
