<?php

namespace App\Console\Commands;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateEventCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Event Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $events = Event::where("end_date", '<', Carbon::now()->setTimezone('Asia/Kathmandu'))
            ->where('status', 1)
            ->update(['status' => 0]);
        if ($events) {
            Log::info("Events were updated");
        }
    }
}
