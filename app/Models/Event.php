<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'start_date', 'description', 'end_date', 'status'];

    protected $dates = ['created_at', 'updated_at'];

    // const STATUS = [
    //     0, // Inactive Finished or not to start
    //     1 // Active
    // ];

    public function scopeFilter($query, $filterType)
    {
        switch ($filterType) {
            case 1: // upcoming event
                return $query->whereDate("start_date", ">", Carbon::now());
                break;
            case 2: // finished event
                return $query->whereDate("end_date", "<", Carbon::now());
                break;
            case 3: // upcoming event within 7 days
                return $query->whereDate("start_date", "<", Carbon::now()->addDays(7))
                    ->whereDate("start_date", ">", Carbon::now());
                break;
            case 4: // finished event with past 7 days
                return $query
                    ->whereDate("end_date", ">", Carbon::now()->subDays(7))
                    ->whereDate("end_date", "<", Carbon::now());
                break;
            case 5: // ongoing events
                return $query->whereDate("start_date", "<=", Carbon::now())
                    ->whereDate("end_date", ">=", Carbon::now());
                break;
            default:
                return $query;
        }
    }
}
