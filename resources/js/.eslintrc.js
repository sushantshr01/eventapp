module.exports = {
  extends: [
    'plugin:vue/strongly-recommended'
  ],
  rules: {
    // override/add rules settings here, such as:
    'vue/max-attributes-per-line': ['error', {
      'singleline': {
        'max': 2
      },
      'multiline': {
        'max': 1,
      }
    }],
    'semi': ['error', 'always'],
    'no-var': ['error'],
    'prefer-const': 1,
    "camelcase": [2, {"properties": "always"}]
  }
};
