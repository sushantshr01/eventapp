import Vue from "vue";
import VueRouter from "vue-router";
import IndexEvent from "./../components/IndexEvent";
import AddEvent from "./../components/AddEvent";

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: "/",
      component: IndexEvent,
      name: "Index"
    },
    {
      path: "/add-event",
      component: AddEvent,
      name: "add_event"
    },
  ],
});
