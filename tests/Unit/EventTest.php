<?php

namespace Tests\Unit;

use App\Models\Event;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class EventTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_filter_upcoming_event()
    {
        Event::factory()->create([
            'start_date' => Carbon::now()->addDay()
        ]);
        Event::factory()->create([
            'start_date' => Carbon::now()->subDay()
        ]);
        $this->assertEquals(2, Event::count());
        $eventCount = Event::filter(1)->count();
        $this->assertEquals(1, $eventCount);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_filter_finished_event()
    {
        Event::factory()->create([
            'end_date' => Carbon::now()->addDay()
        ]);
        Event::factory()->create([
            'end_date' => Carbon::now()->subDay()
        ]);
        $this->assertEquals(2, Event::count());
        $eventCount = Event::filter(2)->count();
        $this->assertEquals(1, $eventCount);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_filter_upcoming_within_seven()
    {
        Event::factory()->create([
            'start_date' => Carbon::now()->addDays(6)
        ]);
        Event::factory()->create([
            'start_date' => Carbon::now()->subDay()
        ]);
        $this->assertEquals(2, Event::count());
        $eventCount = Event::filter(3)->count();
        $this->assertEquals(1, $eventCount);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_filter_finished_past_seven()
    {
        Event::factory()->create([
            'end_date' => Carbon::now()->addDay()
        ]);
        Event::factory()->create([
            'end_date' => Carbon::now()->subDays(6)
        ]);
        $this->assertEquals(2, Event::count());
        $eventCount = Event::filter(4)->count();
        $this->assertEquals(1, $eventCount);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_filter_ongoing_event()
    {
        Event::factory()->create([
            'start_date' => Carbon::now()
        ]);
        Event::factory()->create([
            'end_date' => Carbon::now()->subDay()
        ]);
        $this->assertEquals(2, Event::count());
        $eventCount = Event::filter(5)->count();
        $this->assertEquals(1, $eventCount);
    }
}
