<?php

namespace Tests\Feature\Api;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EventTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * get Event.
     *
     * @return void
     */
    public function test_get_event()
    {
        $event = Event::factory()->create();

        $response = $this->json('GET', '/api/events');

        $response->assertStatus(200);
        $response->assertJsonPath('data.0.title', $event->title);
    }

    /**
     * store Event.
     *
     * @return void
     */
    public function test_store_event()
    {
        $event = Event::factory()->make();

        $response = $this->json('POST', '/api/events', $event->toArray());

        $response->assertStatus(200);
        $response->assertJsonPath('data.title', $event->title);
    }

    /**
     * test required validation.
     *
     * @return void
     */
    public function test_store_event_with_required_validation()
    {
        $response = $this->json('POST', '/api/events', []);
        $response->assertStatus(422)
            ->assertJsonPath("errors.title.0", "The title field is required.")
            ->assertJsonPath("errors.description.0", "The description field is required.")
            ->assertJsonPath("errors.start_date.0", "The start date field is required.")
            ->assertJsonPath("errors.end_date.0", "The end date field is required.");
    }

    /**
     * test date validation.
     *
     * @return void
     */
    public function test_store_event_with_date_validation()
    {
        $event = Event::factory()->make([
            'start_date' => Carbon::now()->subDay(2),
            'end_date' => Carbon::now()->subDay(),
        ]);

        $response = $this->json('POST', '/api/events', $event->toArray());
        $response->assertStatus(422)
            ->assertJsonPath("errors.start_date.0", "The start date must be a date after or equal to " . Carbon::now() . ".")
            ->assertJsonPath("errors.end_date.0", "The end date must be a date after or equal to " . Carbon::now() . ".");
    }

    /**
     * test event update.
     *
     * @return void
     */
    public function test_update_event()
    {
        $event = Event::factory()->create();
        $this->assertEquals($event->status, 1);

        $response = $this->patchJson('/api/events/' . $event->id, [
            'status' => 1
        ]);
        $response->assertStatus(200)
            ->assertJsonPath("data.status", 1);
    }
    /**
     * test event delete.
     *
     * @return void
     */
    public function test_delete_event()
    {
        $event = Event::factory()->count(5)->create();
        $this->assertEquals(Event::count(), 5);

        $response = $this->deleteJson('/api/events/' . $event->first()->id);
        $response->assertStatus(200)
            ->assertJsonPath("data", true);

        $this->assertEquals(Event::count(), 4);
    }
}
