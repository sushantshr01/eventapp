<?php

namespace Database\Seeders;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::factory()->create();
        Event::factory()->create([
            'start_date' => Carbon::now()->subDays(4),
            'end_date' => Carbon::now()->subDay()
        ]);
        Event::factory()->create([
            'start_date' => Carbon::now()->subDays(14),
            'end_date' => Carbon::now()->subDay()
        ]);
        Event::factory()->create([
            'start_date' => Carbon::now()->subDays(14),
            'end_date' => Carbon::now()->subDay(8)
        ]);
        Event::factory()->create([
            'start_date' => Carbon::now()->addDay(),
            'end_date' => Carbon::now()->addDay(4)
        ]);
        Event::factory()->create([
            'start_date' => Carbon::now()->addDays(8),
            'end_date' => Carbon::now()->addDay(14)
        ]);
        Event::factory()->create();
        Event::factory()->create();
    }
}
